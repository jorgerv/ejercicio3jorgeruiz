package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		Scanner teclado=new Scanner(System.in);
		
		System.out.println("Introduce una cadena:");
		String cadena=teclado.nextLine().toLowerCase();
		System.out.println("Tu cadena en minusculas es: "+cadena);
		
		teclado.close();
	}

}
